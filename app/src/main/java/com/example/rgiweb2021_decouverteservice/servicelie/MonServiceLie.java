package com.example.rgiweb2021_decouverteservice.servicelie;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.Objects;

public class MonServiceLie extends Service {

    public MonServiceLie() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d("NICOLAS - Mon service lie", "Methode : onStartCommande");

        return super.onStartCommand(intent, flags, startId);
    }

    public class monBinder extends Binder {

        public MonServiceLie connexionAuServiceLie () {
            return MonServiceLie.this;
        }

    }

    private monBinder leBinder = new monBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return leBinder;
    }

    public int unBeauService(String chaine) {
        if ( Objects.nonNull(chaine) )
                    return chaine.length();
        return 0;
    }
}