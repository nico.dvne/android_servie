package com.example.rgiweb2021_decouverteservice.servicelie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.rgiweb2021_decouverteservice.R;

public class Decouverte_service_lie extends AppCompatActivity {


    private EditText monEditText;
    private TextView resultatTextView;

    private MonServiceLie serveur;

    private ServiceConnection maConnexion = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MonServiceLie.monBinder leBinder = (MonServiceLie.monBinder)service;

            serveur = leBinder.connexionAuServiceLie();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serveur = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decouverte_service_lie);

        monEditText = findViewById(R.id.id_saisie_chaine_edittext);
        resultatTextView = findViewById(R.id.id_resultat_textview);
    }

    public void onClicklDemandeService(View view) {

        String resultat = "";

        String param = monEditText.getText().toString();
        int res = serveur.unBeauService(param);

        resultat = "La chaine contient " + res + " caracteres ";

        resultatTextView.setText(resultat);
    }

    public void onClickLiaisonService(View view) {
        Intent monIntent = new Intent(this, MonServiceLie.class);
        bindService(monIntent, maConnexion, BIND_AUTO_CREATE);
    }

    public void onClickQuitter(View view) {
        finish();
    }
}