package com.example.rgiweb2021_decouverteservice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.rgiweb2021_decouverteservice.servicelie.Decouverte_service_lie;
import com.example.rgiweb2021_decouverteservice.servicesimple.Decouverte_Service_simple;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickDecouverteServiceLie(View view) {
        Intent toServiceLie = new Intent(this, Decouverte_service_lie.class);
        startActivity(toServiceLie);
    }

    public void onClickDecouverteServiceSimple(View view) {
        Intent toServiceSimple = new Intent(this, Decouverte_Service_simple.class);
        startActivity(toServiceSimple);
    }
}