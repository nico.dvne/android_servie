package com.example.rgiweb2021_decouverteservice.servicesimple;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MonServiceSimple extends Service {

    public MonServiceSimple() {
    }

    /**
     *
     * @param intent
     * @return
     */
    @Override
    public IBinder onBind(Intent intent) {
        //Service simple -> on bind return forcement null
        return null;
    }

    /**
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int compteur = 0;

        Runnable monRunnable=()->{
            for(int i = 0 ; i < 10 ; i++ ) {
                Log.d("NICOLAS", "i="+i);

                try { Thread.sleep(750); } catch (InterruptedException e) { e.getMessage();}
            }
        };

        Thread monThread = new Thread(monRunnable);
        monThread.start();

        return super.onStartCommand(intent, flags, startId);
    }
}