package com.example.rgiweb2021_decouverteservice.servicesimple;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.rgiweb2021_decouverteservice.R;

public class Decouverte_Service_simple extends AppCompatActivity {

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decouverte__service_simple);
    }

    /**
     *
     * @param view
     */
    public void onClickQuitter(View view) {
        finish();
    }

    /**
     *
     * @param view
     */
    public void onClickLancerServiceSimple(View view) {

        Intent monIntent = new Intent(this, MonServiceSimple.class);
        startService(monIntent);

    }
}